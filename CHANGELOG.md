## 1.1.0 - 2022-07-13

### Changed

* Detect qualified imports and direct usage as `typing.Union`

## 1.0.0 - 2022-05-31

### Fixed

* Rearranged documentation

## 0.1.0 - 2022-05-08

* Initial release
