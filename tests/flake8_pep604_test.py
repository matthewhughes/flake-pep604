import ast
from textwrap import dedent

import pytest

from flake8_pep604 import Plugin

EXPECTED_MSG = "UNT001 use `|` in place of `typing.Union`. See PEP-604"


@pytest.mark.parametrize("src", ("", "import sys", "from os import environ"))
def test_trivial(src):
    assert list(Plugin(ast.parse(src)).run()) == []


def test_import():
    src = "import typing.Union"

    assert list(Plugin(ast.parse(src)).run()) == [(1, 0, EXPECTED_MSG, Plugin)]


def test_from_import():
    src = "from typing import Union"

    assert list(Plugin(ast.parse(src)).run()) == [(1, 0, EXPECTED_MSG, Plugin)]


def test_qualified_import():
    src = dedent(
        """\
    import typing as ty

    def func(x: ty.Union[int, str]) -> None:
        pass

    def func(y: ty.List[int]) -> None:
        pass
    """
    )

    assert list(Plugin(ast.parse(src)).run()) == [
        (3, 12, EXPECTED_MSG, Plugin),
    ]


def test_indirect_import():
    src = dedent(
        """\
    import typing

    def func(x: typing.Union[int, str]) -> None:
        pass
    """
    )

    assert list(Plugin(ast.parse(src)).run()) == [
        (3, 12, EXPECTED_MSG, Plugin),
    ]


def test_mixed_imports():
    src = dedent(
        """\
    import sys
    import typing.Union

    from os import environ
    from typing import Union
    from typing import cast
    """
    )

    assert list(Plugin(ast.parse(src)).run()) == [
        (2, 0, EXPECTED_MSG, Plugin),
        (5, 0, EXPECTED_MSG, Plugin),
    ]
