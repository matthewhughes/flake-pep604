# Contributing

## Development

Get started by installing everything in a virtualenv

```
$ python -m .venv venv
$ pip install --requirement requirements-dev.txt
$ pre-commit install
```

Then testing:

```
$ pytest
```

And linting:

```
$ pre-commit run --all-files
```
